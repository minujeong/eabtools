import random
import os
import maya.cmds as cmds
from EAB.PIL import Image

class Main:
	# static, range 1 ~ 4096
	width = 1

	# static, range 1 ~ 4096
	height = 1

	# created texture
	created_texture = None

	# called with width, height
	def run_wh(self, width, height):
		Main.width = width
		Maiu.height = height

		self.run();

	# called by button click
	def run(self, e=None):
		print "Creating a debug texture.."

		if self.width < 1 or self.height < 1:
			print "[ERR] width or height can't be less than 1."
			return

		if self.width > 4096 or self.height > 4096:
			print "[ERR] width or height can't be bigger than 4096"
			return

		# force to be integer
		self.width = int(self.width)
		self.height = int(self.height)
		
		Main.created_texture = Image.new("RGBA", (self.width, self.height))
		pixels = Main.created_texture.load()
		for x in range(self.width):
			for y in range(self.height):
				r = random.randrange(0, 256)
				g = random.randrange(0, 256)
				b = random.randrange(0, 256)

				pixels[x, y] = (r, g, b, 255)

		Main.created_texture.save(cmds.internalVar(userScriptDir=True) + ICON_PATH.BASE_PATH + ICON_PATH.RENDER_TEXTURE)

		print "DONE"


class ICON_PATH:
	BASE_PATH = "EAB/Resources/"
	SAMPLE = "sample.png"
	RENDER_TEXTURE = "rendertexture.png"