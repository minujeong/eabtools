"""
This script removes invisible inside-blocks.

author: Minu Jeong (mjung@ea.com)
"""


import maya.cmds as cmds
import maya.OpenMaya as api



class Main:

    ICON = "icons/clear_inside_blocks.png"

    # message:
    def __init__(self):
        self.all_blocks_map = {}
        self.min_x = 0
        self.min_y = 0
        self.min_z = 0
        self.max_x = 0
        self.max_y = 0
        self.max_z = 0

    def __str__(self):
        return "Main Class"

    # public: called from other scripts
    def run(self, e=None):
        blocks = cmds.ls(type="mesh")
        
        print "Clearing inside blocks.."
        for block in blocks:
            self.__register_blocks (block)

        self.__run_remove()
        
        print "Done"


    # private:
    def __run_remove(self):
        directions_to_search = [
            (1, 0, 0),
            (-1, 0, 0),
            (0, 1, 0),
            (0, -1, 0),
            (0, 0, 1),
            (0, 0, -1)
        ]
        
        print "Removing invisible cubes.. %d~%d, %d~%d, %d~%d" % (self.min_x, self.max_x, self.min_y, self.max_y, self.min_z, self.max_z)
        blocks_to_delete = []
        for x in range(self.min_x, self.max_x):
            for y in range(self.min_y, self.max_y):
                for z in range(self.min_z, self.max_z):
                    if not (x, y, z) in self.all_blocks_map:
                        continue
                    
                    
                    block = self.all_blocks_map[(x, y, z)]
                    
                    count = len(directions_to_search)
                    
                    for direction in directions_to_search:
                        a = x + direction[0]
                        b = y + direction[1]
                        c = z + direction[2]
                        
                        if (a, b, c) in self.all_blocks_map:
                            count -= 1
                    
                    if count <= 0:
                        blocks_to_delete.append(block)
                            
                    
        for block_to_delete in blocks_to_delete:
            cmds.delete(block_to_delete)


    def __register_blocks(self, block):
        transform = cmds.listRelatives(block, parent=True)[0]
        point = (cmds.getAttr(transform + ".tx"), cmds.getAttr(transform + ".ty"), cmds.getAttr(transform + ".tz"))
        x = int(point[0])
        y = int(point[1])
        z = int(point[2])
        self.all_blocks_map[(x, y, z)] = transform
        
        if self.min_x > x:
            self.min_x = x
        if self.min_y > y:
            self.min_y = y
        if self.min_z > z:
            self.min_z = z
            
        if self.max_x < x:
            self.max_x = x
        if self.max_y < y:
            self.max_y = y
        if self.max_z < z:
            self.max_z = z

if __name__ == "__main__":
    Main().run()