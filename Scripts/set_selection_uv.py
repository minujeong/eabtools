"""
This script will update all uv coordinates for selected mesh.
Also, this script does not support individual execution: call this script from uified script.

author: Minu Jeong (mjung@ea.com)
"""
import maya.cmds as cmds


class Main:

	ICON = "icons/set_selection_uv.png"

	def __init__(self):
		pass

	def run(self, uu_text, vv_text, e=None):

		uu_str = cmds.textField(uu_text, q=True, text=True)
		vv_str = cmds.textField(vv_text, q=True, text=True)
		
		if uu_str == "":
			uu_str = "0.05"
			cmds.textField(uu_text, e=True, text="0.05")
		
		if vv_str == "":
			vv_str = "0.05"
			cmds.textField(vv_text, e=True, text="0.05")
		
		uu = float(uu_str)
		vv = float(vv_str)

		# Assuming that,
		#  user has selected objects by dragging or clicking the object in the viewport or outliner.
		selected_transforms = cmds.ls(selection=True, type="transform")
		
		# for each selected transforms,
		for transform in selected_transforms:
			mesh = cmds.listRelatives(transform, type="mesh")[0]
			uv_count = cmds.polyEvaluate(mesh, uvcoord=True)
			for index in range(uv_count):
				cmds.polyEditUV("%s.map[%d]" % (mesh, index), relative=False, uValue=uu, vValue=vv)
