import maya.cmds as cmds


class Main:

    ICON = "icons/blocks_counter_by_uv.png"

    def run(self, e=None):
        dags = cmds.ls(dag=True)
        meshes = []

        counter_dict = {}
        

        print "Collecting dag objects.."
        for dag in dags:
            if "uniquePiece" in dag:
                continue

            mesh = cmds.listRelatives(dag, type="mesh")
            if mesh is not None:
                meshes.append(mesh)

        for mesh in meshes:
            u, v = cmds.polyEditUV(mesh[0] + ".map[0]", q=True)

            u = round(u, 2)
            v = round(v, 2)

            # init to dict
            if (u, v) not in counter_dict:
                counter_dict[(u, v)] = 0

            counter_dict[(u, v)] += 1


        
        message = "%d types of bricks.\n" % (len(counter_dict))
        for u, v in counter_dict:
            message += "uv (%s, %s): %d blocks\n" % ("{:4.2f}".format(u), "{:4.2f}".format(v), counter_dict[(u, v)])

        message += "\n"

        cmds.confirmDialog(title="Result", message=message, button=["Close"], defaultButton="Close")

        print "DONE!"