"""
This script reposition objects with float point position.

author: Minu Jeong (mjung@ea.com)
"""

import maya.cmds as cmds


class Main:
	
	ICON = "icons/normalize_object_positions.png"

	def __init__(self):
		pass
	
	def run(self, e=None):
		print "Normalizing blocks.."

		transforms = cmds.ls(type="transform")

		for transform in transforms:
			x, y, z = cmds.getAttr(transform + ".tx"), cmds.getAttr(transform + ".ty"), cmds.getAttr(transform + ".tz")
			x, y, z = round(x), round(y), round(z)

			cmds.setAttr(transform + ".tx", x)
			cmds.setAttr(transform + ".ty", y)
			cmds.setAttr(transform + ".tz", z)

		print "DONE"


if __name__ == "__main__":
	Main().run()
