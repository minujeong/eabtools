import maya.cmds as cmds
import math



class Main:

    ICON = "icons/reconstruct_cubes_by_material.png"

    def __run_by_material(self, material):
        print "New material"

        dags = cmds.ls(dag=True)
        dag_with_mesh = []

        print "Collecting dag objects.."
        for dag in dags:
            if "uniquePiece" in dag:
                continue

            if cmds.listRelatives(dag, type="mesh") is not None:
                dag_with_mesh.append(dag)

        dag_of_material = []
        aa = 0

        print "Finding blocks using shader.."
        for dag in dag_with_mesh:
            shaders = cmds.listConnections(cmds.listHistory(dag,future=1),type='lambert')
            if shaders is None:
                continue

            if shaders[0] == material:
                dag_of_material.append(dag)

        dag_of_clean = []
        
        print "Constructing.."
        for dag in dag_of_material:
            newname = "MAT%s" % (material)
            
            nx, ny, nz, mx, my, mz = cmds.exactWorldBoundingBox(dag)
        
            x = math.floor((nx + mx) / 2.0)
            y = math.floor((ny + my) / 2.0)
            z = math.floor((nz + mz) / 2.0)
        
            # create clean, typical cube
            clean_pCube = cmds.polyCube()[0]
            cmds.move(x, y, z, clean_pCube, absolute=True)
        
            for v in range(0, 14):
                # set uv coordination
                cmds.polyEditUV(clean_pCube + ".map[%d]" % (v), r=False, u=self.uu, v=self.vv)
        
            renamedCube = cmds.rename(clean_pCube, "Cube%s_" % (newname))
            dag_of_clean.append(renamedCube)
        
            # delete possibly rescaled, corrupted mesh
            cmds.delete(dag)

        self.vv += 0.10

        if self.vv > 1.0:
            self.vv = 0.05
            self.uu += 0.10

            if self.uu > 1.0:
                print "[ERROR] Too many materials"
        

    def run(self, e=None):
        self.uu = 0.05
        self.vv = 0.05

        print "[Reconstruction] Please wait. This can be take some time..."

        print "Collecting materials.."
        materials = cmds.ls(materials=True)
        for mat in materials:
            self.__run_by_material(mat)
        print "DONE"