"""
This script remove duplicate positioned bricks.
Execute this script after normalize position.
(While executed from GUI, normalize position script will be called.)

author: Minu Jeong (mjung@ea.com)
"""



import maya.cmds as cmds


class Main:

	ICON = "icons/remove_duplicate_blocks.png"

	def __init__(self):
		pass

	def run(self, e=None):
		print "Removing duplicate blocks.."

		self.cached_blocks = {}
		
		transforms = cmds.ls(type="transform")
		
		is_conflict = False
		duplicateds = []
		for transform in transforms:
			x, y, z = [cmds.getAttr("%s.t%s" % (transform, i)) for i in ("x", "y", "z")]
			
			if (x, y, z) in self.cached_blocks:
				is_conflict = True
				duplicateds.append(transform)
				print "[!!] Duplicate coordination found: ", transform, x, y, z, self.cached_blocks[(x, y, z)]
			else:
				self.cached_blocks[(x, y, z)] = transform

		if not is_conflict:
			print "No duplicate found."
		else:
			for duplicated in duplicateds:
				print "[Delete] Removing %s.." % (duplicated)
				cmds.delete(duplicated)

		print "DONE"


if __name__ == "__main__":
	Main().run()