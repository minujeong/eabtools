"""
This script converts mesh to voxel.
WARNING:
 - Running this script will delete original mesh.)
 - This script uses open maya apis. Running this script will dump a lot of undo stacks, may cause loss of previous undo commands.

author: Minu Jeong (mjung@ea.com)
"""

import math
import time

import maya.cmds as cmds
import maya.OpenMaya as api


class Main:
    # Raycast to outside. make sure this number to be big enough.
    BIG_NUMBER = 10000

    ICON = "icons/voxelize_mesh.png"

    # raycast from point to outside
    def is_point_in_mesh(self, point, mesh):
        # convert string to MFnMesh
        sel = api.MSelectionList()
        sel.add(mesh)
        dagPath = api.MDagPath()
        sel.getDagPath(0, dagPath)
        mesh = api.MFnMesh(dagPath)

        # convert tuple to MFloatPoint
        point = api.MFloatPoint(*point)

        # select any direction
        direction = (0.0, 0.0, 1.0)
        direction = api.MFloatVector(*direction)
        
        # store raycasted result
        farray = api.MFloatPointArray()

        # raycast outside
        mesh.allIntersections(
            point, direction,
            None, None, False,
            api.MSpace.kWorld,
            self.BIG_NUMBER,
            False, None, False,
            farray, None, None, None, None, None
        )

        return farray.length() % 2 == 1


    def set_uv(self, mesh, u, v):
        for m in range(0, 14):
            # set uv coordination
            cmds.polyEditUV(mesh + ".map[%d]" % (m), r=False, u=u, v=v)

    def add_cubes_to_positions(self, positions):
        cubes = cmds.createNode('transform')

        cubes = cmds.rename(cubes, "cubes")

        progress = 0
        count = len(positions)

        for p in positions:

            if progress % 100 == 0:
                
                print "Generating clean cubes briefly %d / %d" % (progress, count)

                time.sleep(0.001)

            progress += 1


            # create poly cube
            pcb = cmds.polyCube()[0]

            cmds.setAttr('%s.tx' % pcb, p[0])
            cmds.setAttr('%s.ty' % pcb, p[1])
            cmds.setAttr('%s.tz' % pcb, p[2])

            # for each vertex, set uv coordination
            self.set_uv(pcb, 0.1, 0.1)

            # set parent container transform
            cmds.parent(pcb, cubes)

            


    def add_spheres_to_positions(self, positions):
        spheres = cmds.createNode('transform')
        
        spheres = cmds.rename(spheres, "spheres")
        
        for p in non_proper_coords:
            pcb = cmds.polySphere()[0]
            
            cmds.setAttr('%s.tx' % pcb, p[0])
            cmds.setAttr('%s.ty' % pcb, p[1])
            cmds.setAttr('%s.tz' % pcb, p[2])
            cmds.setAttr('%s.sx' % pcb, 0.1)
            cmds.setAttr('%s.sy' % pcb, 0.1)
            cmds.setAttr('%s.sz' % pcb, 0.1)

            cmds.parent(pcb, spheres)


    def run_for_each_coord(self, mesh, cubic_width):

        proper_coords = []

        non_proper_coords = []

        # for each x, y, z coordination, search if point is inside mesh
        for x in range(-cubic_width, cubic_width):

            print "Evaluating world space %d / %d Processing.." % (x + cubic_width + 1, cubic_width * 2)

            time.sleep(0.001)

            for y in range(0, cubic_width * 2):
                for z in range(-cubic_width, cubic_width):
                    if time.time() - self.start_time > self.timer:

                        print "Time out"

                        return ([], [])

                    p = (x, y, z)

                    if self.is_point_in_mesh(p, mesh):
                        proper_coords.append(p)

                    else:
                        non_proper_coords.append(p)


        return (proper_coords, non_proper_coords)


    # this is main procedure
    def run(self, size, timer, e=None):

        def timeup(signum, frame):
            if running:
                raise Exception("Timed out")

        # get any mesh
        meshes = cmds.ls(type='mesh')

        if len(meshes) == 0:
            print "No mesh found"
            return

        mesh = meshes[0]

        cubic_width = int(math.ceil(size * 10.0) / 10.0)

        
        # time escape
        self.timer = timer

        self.start_time = time.time()


        print "Evaluating world... option: size - %d in time - %d .." % (cubic_width, timer)


        (proper_coords, non_proper_coords) = self.run_for_each_coord(mesh, cubic_width)
        
        if len(proper_coords) > 0:
            # delete model mesh
            cmds.delete(mesh)

            self.add_cubes_to_positions (proper_coords)
        
        else:
            print "Failed voxelizing"
        

        print "Done in time.. %.2f seconds" % (time.time() - self.start_time)

        print "DONE"


if __name__ == "__main__":
    Main().run()