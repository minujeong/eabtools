import maya.cmds as cmds


class Main:

    ICON = "icons/blocks_counter_by_material.png"

    def run(self, e=None):
        counter_dict = {}

        ignore_list = ["particleCloud1", "lambert1"]

        print "Collecting materials.."
        materials = cmds.ls(materials=True)
        for mat in materials:
            if mat in ignore_list:
                continue

            counter_dict[mat] = 0

        dags = cmds.ls(dag=True)
        dag_with_mesh = []

        print "Collecting dag objects.."
        for dag in dags:
            if "uniquePiece" in dag:
                continue

            if cmds.listRelatives(dag, type="mesh") is not None:
                dag_with_mesh.append(dag)

        print "Analyzing dags by materials.."
        for dag in dag_with_mesh:
            shaders = cmds.listConnections(cmds.listHistory(dag,future=1), type='lambert')
            if shaders is None:
                continue

            if shaders[0] not in counter_dict:
                counter_dict[shaders[0]] = 0

            counter_dict[shaders[0]] += 1

        
        message = "%d types of bricks.\n" % (len(counter_dict))
        
        for mat, count in counter_dict.iteritems():
            message += "%s: %d blocks\n" % (mat, count)

        message += "\n"

        cmds.confirmDialog(title="Result", message=message, button=["Close"], defaultButton="Close")

        print "DONE!"