"""
Copy scripts in 'Tools/EAB' directory to maya python site-packages

This script displayes GUI window for other scripts.
Although many scripts are able to run inidividually, this script allows users to use easily.

author: Minu Jeong
"""


from EAB.Scripts import voxelize_mesh, clear_inside_blocks
from EAB.Scripts import normalize_object_positions, set_selection_uv
from EAB.Scripts import remove_duplicate_blocks
from EAB.Scripts import reconstruct_cubes_by_material
from EAB.Scripts import create_texture
from EAB.Scripts import blocks_counter_by_uv
from EAB.Scripts import blocks_counter_by_material

import maya.cmds as cmds
import maya.utils as utils
import maya.mel as mel

from functools import partial


# DEBUG
reload(voxelize_mesh)
reload(clear_inside_blocks)
reload(normalize_object_positions)
reload(set_selection_uv)
reload(remove_duplicate_blocks)
reload(reconstruct_cubes_by_material)
reload(create_texture)
reload(blocks_counter_by_uv)
reload(blocks_counter_by_material)


class VERSION:
    MAJOR = 1
    MINOR = 1
    RELEASE = 1

    def __str__(self):
        return "".join([str(VERSION.MAJOR), ".", str(VERSION.MINOR), ".", str(VERSION.RELEASE)])


# Start point of GUI window
class Main:
    
    BASE_PATH = "EAB/Resources/"
    TITLE_ICON = "title.png"

    def __init__(self):

        print "Loading EAB Toolset..."

        Main.BASE_PATH = cmds.internalVar(userScriptDir=True) + Main.BASE_PATH

        print " - Base path is set to: %s" % (Main.BASE_PATH)

        print VERSION()

        self.window_name = "EAB Toolset"

        if cmds.window(self.window_name, exists=True):
            cmds.deleteUI(self.window_name)
        

        Main.WINDOW_WIDTH = 262
        Main.ROW_HEIGHT = 25

        self.win = cmds.window(self.window_name, resizeToFitChildren=True, sizeable=True,
                               minimizeButton=False, maximizeButton=False)
        
        self.base_layout = cmds.rowColumnLayout(numberOfColumns=1)

        self.tools_layout = cmds.rowColumnLayout(parent=self.base_layout, numberOfColumns=1)

        
        # Title Image
        cmds.image(image=Main.BASE_PATH + Main.TITLE_ICON, parent=self.tools_layout, width=Main.WINDOW_WIDTH, height=Main.ROW_HEIGHT * 2)
        


        #############################################
        # Voxelize Tools
        #############################################

        # setup
        self.voxelizeTools = cmds.frameLayout("Voxelize Tools", parent=self.tools_layout, collapsable=True, width=Main.WINDOW_WIDTH, borderStyle="in")

        voxelize_tools_item_width = 128
        voxelize_tools_item_height = 49

        self.voxelizeOptions = cmds.rowColumnLayout("Options", parent=self.voxelizeTools, numberOfColumns=2)


        # size option
        sizeOption = cmds.columnLayout(parent=self.voxelizeOptions)

        vxop_size_label = cmds.text(parent=sizeOption, label="Size: 0 units",
                  width=voxelize_tools_item_width)


        self.vxop_size = cmds.floatSlider(parent=sizeOption, value=10, minValue=1, maxValue=20,
                  width=voxelize_tools_item_width,
                  changeCommand=lambda e: cmds.text(vxop_size_label, edit=True, label="Size: %d unit" % (cmds.floatSlider(self.vxop_size, q=True, value=True))))

        cmds.text(vxop_size_label, edit=True, label="Size: %d unit" % (cmds.floatSlider(self.vxop_size, q=True, value=True)))


        timerOption = cmds.columnLayout(parent=self.voxelizeOptions)

        vxop_timer_label = cmds.text(parent=timerOption, label="Max Run Time: 0 secs",
                  width=voxelize_tools_item_width)


        self.vxop_timer = cmds.floatSlider(parent=timerOption, value=15, minValue=3, maxValue=60,
                  width=voxelize_tools_item_width,
                  changeCommand=lambda e: cmds.text(vxop_timer_label, edit=True, label="Max Run Time: %d secs" % (cmds.floatSlider(self.vxop_timer, q=True, value=True))))

        cmds.text(vxop_timer_label, edit=True, label="Max Run Time: %d secs" % (cmds.floatSlider(self.vxop_timer, q=True, value=True)))


        def confirm_voxelize(e):
            size = cmds.floatSlider(self.vxop_size, q=True, value=True)
            timer = cmds.floatSlider(self.vxop_timer, q=True, value=True)

            voxelize_mesh.Main().run(size, timer)

        # confirm button
        cmds.symbolButton(image=Main.BASE_PATH + voxelize_mesh.Main.ICON, parent=self.voxelizeTools,
                          width=voxelize_tools_item_width, height=voxelize_tools_item_height,
                          command=confirm_voxelize)


        cmds.text("", parent=self.voxelizeTools)



        #############################################
        # Edit Tools
        #############################################
        self.editTools = cmds.frameLayout("Edit Tools", parent=self.tools_layout, collapsable=True, width=Main.WINDOW_WIDTH, borderStyle="in")

        cmds.text(parent=self.editTools, label="Edit Tools", font="boldLabelFont", align="left", height=Main.ROW_HEIGHT)

        edit_tools_icon_width = 128
        edit_tools_icon_height = 49

        self.edit_tool_icons_container = cmds.rowColumnLayout("Icons", parent=self.editTools, numberOfColumns=2)

        cmds.symbolButton(image=Main.BASE_PATH + clear_inside_blocks.Main.ICON, parent=self.edit_tool_icons_container,
            width=edit_tools_icon_width, height=edit_tools_icon_height, command=clear_inside_blocks.Main().run)

        cmds.symbolButton(image=Main.BASE_PATH + reconstruct_cubes_by_material.Main.ICON, parent=self.edit_tool_icons_container,
            width=edit_tools_icon_width, height=edit_tools_icon_height, command=reconstruct_cubes_by_material.Main().run)

        cmds.symbolButton(image=Main.BASE_PATH + normalize_object_positions.Main.ICON, parent=self.edit_tool_icons_container,
            width=edit_tools_icon_width, height=edit_tools_icon_height, command=normalize_object_positions.Main().run)

        cmds.symbolButton(image=Main.BASE_PATH + remove_duplicate_blocks.Main.ICON, parent=self.edit_tool_icons_container,
            width=edit_tools_icon_width, height=edit_tools_icon_height, command=remove_duplicate_blocks.Main().run)


        cmds.text("", parent=self.editTools)



        #############################################
        # Counting Tools
        #############################################

        # setup
        self.countingTools = cmds.frameLayout("Counting Tools", parent=self.tools_layout, collapsable=True, width=Main.WINDOW_WIDTH, borderStyle="in")

        counting_tools_item_width = 128
        counting_tools_item_height = 49

        self.counting_tools_container = cmds.rowColumnLayout("counting tool icons", parent=self.countingTools, numberOfColumns=2)


        cmds.symbolButton(image=Main.BASE_PATH + blocks_counter_by_uv.Main.ICON, parent=self.counting_tools_container,
            width=edit_tools_icon_width, height=edit_tools_icon_height, command=blocks_counter_by_uv.Main().run)

        cmds.symbolButton(image=Main.BASE_PATH + blocks_counter_by_material.Main.ICON, parent=self.counting_tools_container,
            width=edit_tools_icon_width, height=edit_tools_icon_height, command=blocks_counter_by_material.Main().run)



        #############################################
        # UV Tools
        #############################################

        self.uv_tools = cmds.frameLayout("UV Tools", parent=self.tools_layout, collapsable=True, collapse=False, width=Main.WINDOW_WIDTH)

        cmds.text(parent=self.uv_tools, label="UV Tools", font="boldLabelFont", align="left", height=Main.ROW_HEIGHT)


        # defining width and height
        desc_width = Main.WINDOW_WIDTH * 0.05
        value_width = Main.WINDOW_WIDTH * 0.45

        cmds.text(label="Select DAG Objects with mesh", parent=self.uv_tools, align="center", font="tinyBoldLabelFont", backgroundColor=(0.0, 0.0, 0.0))

        self.uv_input_layout = cmds.rowColumnLayout(parent=self.uv_tools, numberOfColumns=4, columnWidth=[(1, desc_width), (2, value_width),
                                                                                                          (3, desc_width), (4, value_width)])

        cmds.text(label="U ", parent=self.uv_input_layout, align="right")

        self.text_field_u = cmds.textField("U", parent=self.uv_input_layout, width=value_width, height=Main.ROW_HEIGHT, placeholderText="0.0~1.0")

        cmds.text(label="V ", parent=self.uv_input_layout, align="right")

        self.text_field_v = cmds.textField("V", parent=self.uv_input_layout, width=value_width, height=Main.ROW_HEIGHT, placeholderText="0.0~1.0")
        
        cmds.symbolButton(image=Main.BASE_PATH + set_selection_uv.Main.ICON, parent=self.uv_tools, height=48,
                          command=partial(set_selection_uv.Main().run, self.text_field_u, self.text_field_v))

        cmds.text(label="", parent=self.uv_tools)
        


        #############################################
        # Texture Tools
        #############################################

        self.texture_tools = cmds.frameLayout("Texture Tools [develop]", parent=self.tools_layout, collapsable=True, collapse=True, width=Main.WINDOW_WIDTH, marginWidth=4)

        cmds.text(parent=self.texture_tools, label="Texture Tools", font="boldLabelFont", align="left", height=Main.ROW_HEIGHT)

        
        desc_width = (Main.WINDOW_WIDTH - 16) * 0.17

        spacing = (Main.WINDOW_WIDTH - 16) * 0.06

        value_width = (Main.WINDOW_WIDTH - 16) * 0.27

        self.texture_tools_width_height_setup_layout = cmds.rowColumnLayout(parent=self.texture_tools, numberOfColumns=5,
                                                       columnWidth=[(1, desc_width), (2, value_width),
                                                                    (3, spacing),
                                                                    (4, desc_width), (5, value_width)])

        cmds.text(label="width", parent=self.texture_tools_width_height_setup_layout, align="center")

        self.new_texture_width_value = cmds.textField("Width", parent=self.texture_tools_width_height_setup_layout, placeholderText="20")

        cmds.text(label="", parent=self.texture_tools_width_height_setup_layout);

        cmds.text(label="height", parent=self.texture_tools_width_height_setup_layout, align="center")

        self.new_texture_height_value = cmds.textField("Height", parent=self.texture_tools_width_height_setup_layout, placeholderText="20")


        cmds.button("Create Textures", parent=self.texture_tools, width=Main.WINDOW_WIDTH - 16, height=Main.ROW_HEIGHT, command=self.create_new_texture)

        cmds.text(label="", parent=self.texture_tools)



        #############################################
        # Painting Tools
        #############################################

        self.painting_tools = cmds.frameLayout("Painting [develop]", parent=self.tools_layout, collapsable=True, collapse=True, width=Main.WINDOW_WIDTH - 4, marginWidth=4)

        cmds.text(parent=self.painting_tools, label="Color Setup", font="boldLabelFont", align="left", height=Main.ROW_HEIGHT)
        

        self.layout_dev_tools = cmds.rowColumnLayout(parent=self.painting_tools, numberOfColumns=2, columnWidth=[(1, (Main.WINDOW_WIDTH - 16) * 0.25),
                                                                                                                 (2, (Main.WINDOW_WIDTH - 16) * 0.75)])
        
        cmds.text("New Name ", parent=self.layout_dev_tools, align="right")

        self.text_field_new_brick_name = cmds.textField("BrickName", parent=self.layout_dev_tools, height=Main.ROW_HEIGHT)

        #### call self.rename_selections

        cmds.text(label="", parent=self.layout_dev_tools)



        #############################################
        # NOT USED
        #############################################

        self.devTools = cmds.frameLayout("Not Used [develop]", parent=self.tools_layout, collapsable=True, collapse=True, width=Main.WINDOW_WIDTH, marginWidth=4)

        self.vertex_color_slider_group = cmds.colorSliderGrp("Color Slider", parent=self.devTools)
        cmds.button("Paint", parent=self.devTools, width=Main.WINDOW_WIDTH - 8, height=Main.ROW_HEIGHT, command=self.toggle_paint)

        cmds.text(label="", parent=self.devTools)



        #############################################
        # Show Window
        #############################################
        cmds.showWindow(self.win)

        print "EAB Toolset loaded!"


    # setup uv
    def rename_selections(self, e=None):

        new_brick_name = cmds.textField(self.text_field_new_brick_name, query=True, text=True)
        
        selecteds = cmds.ls(selection=True)
        
        index = 0

        for selected in selecteds:
            new_name = "%s_%d" % (new_brick_name, index)
            
            cmds.rename(selected, new_name)
            index += 1
        
        cmds.textField(self.text_field_new_brick_name, text="", edit=True)


    # Update input height and run
    def create_new_texture(self, e=None):
        set_width =  cmds.textField(self.new_texture_width_value, query=True, text=True)
        set_height = cmds.textField(self.new_texture_height_value, query=True, text=True)

        if set_width != "":
            create_texture.Main.width = int(set_width)
        else:
            create_texture.Main.width = 20
        
        if set_height != "":
            create_texture.Main.height = int(set_height)
        else:
            create_texture.Main.height = 20


        create_texture.Main().run()


    # dev
    def toggle_paint(self, e=None):
        print "[DEV] Under development. nothing will happen"



if __name__ == "__main__":
    Main()